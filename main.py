from imports import *
#import sys,os
#from PyQt5.QtWidgets import QApplication,QCheckBox, QWidget, QLabel, QLineEdit, QPushButton, QVBoxLayout, QMessageBox
#from PyQt5.QtGui import QIcon
#import re
#from datetime import timedelta
#from convert_minute_to_string import convert_minute_to_string

class MyWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Time Logger')
        # Replace 'path_to_icon.ico' with the actual path to your icon file
        icon_path = os.path.abspath("clock_icon.png")
        self.setWindowIcon(QIcon(icon_path))  
        
        self.label = QLabel("Enter time (hh:mm):")
        self.entry_time = QLineEdit()
        self.label_date = QLabel("Enter date (dd-mm-yy):")
        self.entry_date = QLineEdit()
        self.include_pause_checkbox = QCheckBox("Pause included")
        self.include_decimal_checkbox = QCheckBox("Decimal timing(not implemented yet)")
        self.add_button = QPushButton("Add")
        self.add_button.clicked.connect(self.add_time)
        self.display_button = QPushButton("Display File Content")
        self.display_button.clicked.connect(self.display_file_content)
        self.quit_button = QPushButton("Quit")
        self.quit_button.clicked.connect(self.close)
        
        self.total_time_label = QLabel("Total Time Worked:")
        self.total_extra_label = QLabel("Total Extra Time:")

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.entry_time)
        layout.addWidget(self.label_date)
        layout.addWidget(self.entry_date)
        layout.addWidget(self.include_pause_checkbox)
        layout.addWidget(self.include_decimal_checkbox)
        layout.addWidget(self.add_button)
        layout.addWidget(self.display_button)
        layout.addWidget(self.total_time_label)
        layout.addWidget(self.total_extra_label)
        layout.addWidget(self.quit_button)

        self.setLayout(layout)
        self.calculate_totals()

    def add_time(self):
        entered_time = self.entry_time.text()
        entered_date = self.entry_date.text()
        if not entered_time or not entered_date:
            QMessageBox.warning(self, "Warning", "Enter both time and date")
            return
        # Check if entered date and time match the specified format
        if not re.match(r'^\d{2}-\d{2}-\d{2}$', entered_date):
            QMessageBox.warning(self, "Warning", "Enter date in format dd-mm-yy")
            return
        if not re.match(r'^\d{2}:\d{2}$', entered_time):
            QMessageBox.warning(self, "Warning", "Enter time in format hh:mm")
            return
        
        # Get the state of the checkbox
        pause_included = self.include_pause_checkbox.isChecked()
        # Determine the pause info
        pause_info = "INC" if pause_included else "NIC"

        with open('time_log.txt', 'a') as file:
            file.write(f"{entered_date} - {entered_time} - {pause_info}\n")

        QMessageBox.information(self, "Success", "Time added successfully")
        self.calculate_totals()

    def calculate_totals(self):
        total_worked = timedelta()
        print(f"total worked = {total_worked}")
        total_extra = timedelta()
        total_minutes = 0
        total_hour_minutes = 0
        num_lines = sum(1 for _ in open('time_log.txt'))
        try:
            with open('time_log.txt', 'r') as file:
                lines = file.readlines()
                for line in lines:
                    print(f"total_minutes begin loop = {total_minutes}")
                    parts = line.strip().split(" - ")
                    date_str, time_str, pause_info = parts
                    print(f"time string = {time_str}")
                    time_parts = time_str.split(":")
                    hours = int(time_parts[0])
                    print(f"hours = {hours}")
                    minutes = int(time_parts[1])
                    pause_info = str(parts[2])
                    print(f"Pasuse = {pause_info}")
                    if pause_info == "NIC":
                        pause_time=150
                    else:
                        pause_time=0
                    print(f"Mola time = {pause_time}")                  
                    total_hour_minutes = hours * 60
                    total_minutes = total_minutes+ total_hour_minutes + minutes + pause_time
                    print(f"total_minutes = {total_minutes}")
                    

            total_extra=calculate_difference(total_minutes,num_lines)
            total_extra_string=convert_minute_to_string(total_extra)

            total_worked=convert_minute_to_string(total_minutes)
            # Update labels
            self.total_time_label.setText(f"Total Time Worked: {str(total_worked)}")
            self.total_extra_label.setText(f"Total Extra Time: {str(total_extra_string)}")
        
        except FileNotFoundError:
            QMessageBox.warning(self, "FILE NOT FOUND")
 
         
    def display_file_content(self):
        try:
            with open('time_log.txt', 'r') as file:
                content = file.read()
                if content:
                    QMessageBox.information(self, "File Content", content)
                else:
                    QMessageBox.information(self, "File Content", "File is empty")
        except FileNotFoundError:
            QMessageBox.warning(self, "File Not Found", "No log file found")

def main():
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('clock_icon.png'))
    widget = MyWidget()
    widget.show()
    sys.exit(app.exec_())
    
if __name__ == "__main__":
    main()
