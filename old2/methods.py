import os
import sys
from datetime import datetime, timedelta
from PyQt5.QtWidgets import QMessageBox

def restart_program():
    python = sys.executable
    os.execl(python, python, *sys.argv)

def add_time(entry_time_str, entry_date_str):
    # Add functionality for adding time
    with open('time_log.txt', 'a') as file:
        entry_text = f"{entry_date_str} - {entry_time_str}\n"
        file.write(entry_text)
    restart_program()

def check_entry(entry_time, entry_date):
    if entry_time == "" or entry_date == "":
        QMessageBox.warning(None, "Warning", "Enter time and date")
        restart_program()
    else:
        add_time(entry_time, entry_date)

def save_and_quit():
    # Save total_difference to a file or database
    # For simplicity, let's just print it here
    print("Final Total Difference:", self.total_difference)
    self.root.destroy()

def read_and_display_file(file_content):
    filename = "time_log.txt"
    if os.path.exists(filename):
        with open(filename, 'r') as file:
            content = file.read()
            file_content.setText(f"File Content: \n {content}")
            parse_the_content(content)

def parse_the_content(content):
    total_lines = 0
    # Process each line separately
    for line in content.split("\n"):
        if not line:
            continue  # Skip empty lines
        total_lines += 1
        line_parts = line.split("-")
        time_str = line_parts[-1].strip()
        if len(time_str) >= 2:
            hours, minutes = map(int, time_str.split(':'))
            total_hours += hours
            total_minutes += minutes
            remaining_minutes += minutes
            if remaining_minutes >= 60:
                total_hours += 1
                remaining_minutes -= 60
        else:
            print(f"Skipped line: {line}")

    print(f"Total logged in time: {total_hours}:{remaining_minutes}")
    required_min_minutes = total_lines * 2550
    print(f"Required min minutes : {required_min_minutes}")
    acquired_total_minutes = (total_hours * 60) + remaining_minutes
    print(f"Acquired total minutes : {acquired_total_minutes}")
    rest_total_minutes = acquired_total_minutes - required_min_minutes
    if rest_total_minutes > 0:
        status_text = "Acquired is more"
    else:
        status_text = "Required is more"
    print(f"Rest total minutes : {rest_total_minutes}")
    rest_hours = rest_total_minutes // 60
    rest_minutes = rest_total_minutes % 60
    print(f"Rest hours = {rest_hours}, Rest minutes = {rest_minutes}")
    print(f"Total Difference: {rest_hours:02d}:{rest_minutes:02d} --> {status_text}")


'''
import os
from datetime import datetime, timedelta

class MyMethods:
    total_lines = 0
    def __init__(self):
        # Initialize variables
        self.total_difference = timedelta()
        self.total_hours = 0
        self.total_minutes = 0
        self.remaining_minutes = 0
        #self.total_lines = 0
        self.required_min_minutes = 0
        self.rest_total_minutes = 0
        self.rest_minutes = 0
        self.rest_hours = 0
        self.status_text  = ""
    
    @staticmethod
    def restart_program():
        python = sys.executable
        os.execl(python, python, *sys.argv)

    @staticmethod
    def add_time(entry_time_str, entry_date_str):
        # Add functionality for adding time
        with open('time_log.txt', 'a') as file:
            entry_text = f"{entry_date_str} - {entry_time_str}\n"
            file.write(entry_text)
        MyMethods.restart_program()

    @staticmethod
    def check_entry(entry_time, entry_date):
        if entry_time == "" or entry_date == "":
            messagebox.showwarning("Warning", "Enter time and date")
            MyMethods.restart_program()
        else:
            MyMethods.add_time(entry_time, entry_date)

    @staticmethod
    def save_and_quit():
        # Save total_difference to a file or database
        # For simplicity, let's just print it here
        print("Final Total Difference:", self.total_difference)
        self.root.destroy()

    @staticmethod
    def read_and_display_file(file_content):
        filename = "time_log.txt"
        if os.path.exists(filename):
            with open(filename, 'r') as file:
                content = file.read()
                file_content.setText(f"File Content: \n {content}")
                MyMethods.parse_the_content(content)
    
    def parse_the_content(content):
        lines=content.split("\n")
        # Process each line separately
        for line in lines:
            if not line:
                continue  # Skip empty lines
            # Split the line into date and hour components
            MyMethods.total_lines += 1
            line_parts = line.split("-")
            time_str = line_parts[-1].strip()
            # Check if there are enough parts to unpack
            if len(time_str) >= 2:
                hours, minutes = map(int, time_str.split(':'))
                # Process the date and hour as needed
                # For example, display them on labels
                #self.content_hours.config(text=f"Last entry hour: {hours}")
                #print(f"Loop hours : {hours}")
                #self.add_hours(hours)
                MyMethods.add_minutes(hours*60)
                self.add_minutes(minutes)
                #print(f"Loop minutes : {minutes}")
                
            else:
                # Handle lines without the expected format
                print(f"Skipped line: {line}")
                self.content_hours.config(text=f"Last entry hour: {hours}")
        self.min_to_hour(self.total_minutes)
        self.content_hours.config(text=f"Total logged in time: {self.total_hours}:{self.remaining_minutes}")
        self.required_min_minutes= self.total_lines*2550
        print(f"required min minutes : {self.required_min_minutes}")
        self.acquired_total_minutes = (self.total_hours*60)+self.remaining_minutes
        print(f"acquired total minutes : {self.acquired_total_minutes}")
        self.rest_total_minutes = self.acquired_total_minutes - self.required_min_minutes
        if (self.rest_total_minutes > 0):
            self.status_text = "Acquired is more"
        else:
            self.status_text = "Required is more"
        print(f"rest total minutes : {self.rest_total_minutes}")
        self.rest_hours = self.rest_total_minutes // 60
        print(f"rest_hours = {self.rest_hours}")
        self.rest_minutes = self.rest_total_minutes % 60
        print(f"rest_minutes = {self.rest_minutes}")
        self.rest_time.config(text=f"Total Difference: {self.rest_hours:02d}:{self.rest_minutes:02d} --> {self.status_text}")
'''

'''   
def parse_the_content(self, content):
    lines=content.split("\n")
        # Process each line separately
    for line in lines:
        if not line:
            continue  # Skip empty lines
        # Split the line into date and hour components
        self.total_lines += 1
        line_parts = line.split("-")
        time_str = line_parts[-1].strip()
        # Check if there are enough parts to unpack
        if len(time_str) >= 2:
            hours, minutes = map(int, time_str.split(':'))
            # Process the date and hour as needed
            # For example, display them on labels
            #self.content_hours.config(text=f"Last entry hour: {hours}")
            #print(f"Loop hours : {hours}")
            #self.add_hours(hours)
            self.add_minutes(hours*60)
            self.add_minutes(minutes)
            #print(f"Loop minutes : {minutes}")
            
        else:
            # Handle lines without the expected format
            print(f"Skipped line: {line}")
            self.content_hours.config(text=f"Last entry hour: {hours}")
    self.min_to_hour(self.total_minutes)
    self.content_hours.config(text=f"Total logged in time: {self.total_hours}:{self.remaining_minutes}")
    self.required_min_minutes= self.total_lines*2550
    print(f"required min minutes : {self.required_min_minutes}")
    self.acquired_total_minutes = (self.total_hours*60)+self.remaining_minutes
    print(f"acquired total minutes : {self.acquired_total_minutes}")
    self.rest_total_minutes = self.acquired_total_minutes - self.required_min_minutes
    if (self.rest_total_minutes > 0):
        self.status_text = "Acquired is more"
    else:
        self.status_text = "Required is more"
    print(f"rest total minutes : {self.rest_total_minutes}")
    self.rest_hours = self.rest_total_minutes // 60
    print(f"rest_hours = {self.rest_hours}")
    self.rest_minutes = self.rest_total_minutes % 60
    print(f"rest_minutes = {self.rest_minutes}")
    self.rest_time.config(text=f"Total Difference: {self.rest_hours:02d}:{self.rest_minutes:02d} --> {self.status_text}")

def read_and_display_file():
    filename= "time_log.txt"
    if os.path.exists(filename):
        with open(filename,'r') as file:
            content = file.read()
            file_content.config(text=f"File Content: \n {content} ")
            parse_the_content(content)

'''