import sys, os 
from methods import MyMethods
import os
from datetime import datetime, timedelta
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QWidget, QLabel, QPushButton, QLineEdit, QVBoxLayout, QGroupBox
#from work_calculator import WorkCalculator


class MyWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.label = QLabel("Enter time (hh:mm):")
        self.entry_time = QLineEdit()
        self.add_button = QPushButton("Add")
        self.result_label = QLabel("Total Difference: 00:00")
        self.label_date = QLabel("Last entry date:")
        self.entry_date = QLineEdit()
        self.file_content = QLabel("a")
        self.content_hours = QLabel("h")
        self.rest_time = QLabel("Total Difference 00:00")
        
        first_input_layout = QHBoxLayout()
        first_input_layout.addWidget(self.label)
        first_input_layout.addWidget(self.entry_time)
        
        second_input_layout = QHBoxLayout()
        second_input_layout.addWidget(self.label_date)
        second_input_layout.addWidget(self.entry_date)
        
        #grouping layouts in panel
        input_panel=QGroupBox("Input")
        panel_layout=QVBoxLayout()
        panel_layout.addLayout(first_input_layout)
        panel_layout.addLayout(second_input_layout)
        panel_layout.addWidget(self.add_button)
        input_panel.setLayout(panel_layout)

        layout = QVBoxLayout()
        layout.addWidget(input_panel)
        layout.addWidget(self.result_label)
        layout.addWidget(self.file_content)
        layout.addWidget(self.content_hours)
        layout.addWidget(self.rest_time)
        
        self.setLayout(layout)
        self.add_button.clicked.connect(self.add_button_clicked)

    def add_button_clicked(self):
        entry_time_str = self.entry_time.text()
        entry_date_str = self.entry_date.text()
        MyMethods.check_entry(entry_time_str, entry_date_str)


if __name__ == "__main__":
    #methods.read_and_display_file()
    app = QApplication(sys.argv)
    widget = MyWidget()
    MyMethods.read_and_display_file(widget.file_content)
    widget.show()
    sys.exit(app.exec_())