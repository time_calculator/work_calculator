import os
import sys
import tkinter as tk
from work_calculator import WorkCalculator


if __name__ == "__main__":
    root = tk.Tk()
    app = WorkCalculator(root)
    root.mainloop()

    # Configure the close event
    root.protocol("WM_DELETE_WINDOW", app.save_and_quit)

    root.mainloop()
