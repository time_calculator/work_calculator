import os
import sys
import tkinter as tk
from datetime import datetime, timedelta
from tkinter import messagebox

class WorkCalculator:
    def __init__(self, root):
        self.root = root
        self.root.title("Time Tracker")
        

        # Initialize variables
        self.total_difference = timedelta()
        self.total_hours = 0
        self.total_minutes = 0
        self.remaining_minutes = 0
        self.total_lines = 0
        self.required_min_minutes = 0
        self.rest_total_minutes = 0
        self.rest_minutes = 0
        self.rest_hours = 0
        self.status_text  = ""
        
        # Create widgets
        self.label = tk.Label(root, text="Enter time (hh:mm):")
        self.entry_time = tk.Entry(root)
        self.add_button = tk.Button(root, text="Add", command=self.check_entry)
        self.result_label = tk.Label(root, text="Total Difference: 00:00")
        self.label_date = tk.Label(root, text="Last entry date:")
        self.entry_date = tk.Entry(root)
        self.file_content = tk.Label(root, text="a")
        self.content_hours = tk.Label(root, text="h")
        self.rest_time = tk.Label(root, text="Total Difference 00:00")
        
        #Arrange widgets location
        self.label.grid(row=0, column=0,padx=10,pady=10,sticky=tk.E)
        self.entry_time.grid(row=1, column=0,padx=10,pady=10)
        self.label_date.grid(row=0, column=1,padx=10,pady=10,sticky=tk.E)
        self.entry_date.grid(row=1, column=1,padx=10,pady=10)
        self.add_button.grid(row=3, column=0,columnspan=2,pady=10)
        self.result_label.grid(row=4, column=0,columnspan=2,pady=10,sticky=tk.E)
        self.file_content.grid(row=5,column=1,columnspan=2,pady=10,sticky=tk.E)
        self.content_hours.grid(row=5,column=0,columnspan=1,pady=10,sticky=tk.E)
        self.rest_time.grid(row=6,column=0,columnspan=1,pady=5,sticky=tk.E)
        
            # Debugging output for widget visibility
        print("Label visibility:", self.label.winfo_ismapped())
        print("Entry visibility:", self.entry_time.winfo_ismapped())
        print("Add button visibility:", self.add_button.winfo_ismapped())
        
        
        # Check if the file exists and display its content
        self.read_and_display_file()

    def restart_program(self):
        python = sys.executable
        os.execl(python, python, * sys.argv)
        
    def add_time(self):
        entered_time_str = self.entry_time.get()
        print (f"entered_time = {entered_time_str}")
        entry_date_str = self.entry_date.get()
        print (f"entered_date = {entry_date_str}")
        
        #try:
        #    entered_time = datetime.strptime(entered_time_str, "%H:%M").time()
        #except ValueError:
        #    # Handle invalid time format
        #    return
        
        with open('time_log.txt', 'a') as file:
            entry_text = f"{entry_date_str} - {entered_time_str}\n"
            file.write(entry_text)
            file.close()
        self.restart_program()
        
    def check_entry(self):
        entered_time = self.entry_time.get()
        entry_date = self.entry_date.get()
        if entered_time=="" or entry_date=="":
            messagebox.showwarning("Warning", "enter time and date")
            self.restart_program()
        else:
            self.add_time()
            
    def save_and_quit(self):
        # Save total_difference to a file or database
        # For simplicity, let's just print it here
        print("Final Total Difference:", self.total_difference)
        self.root.destroy()
        
    def read_and_display_file(self):
        filename= "time_log.txt"
        if os.path.exists(filename):
            with open(filename,'r') as file:
                content = file.read()
                self.file_content.config(text=f"File Content: \n {content} ")
                self.parse_the_content(content)
    
    def parse_the_content(self, content):
        lines=content.split("\n")
         # Process each line separately
        for line in lines:
            if not line:
                continue  # Skip empty lines
            # Split the line into date and hour components
            self.total_lines += 1
            line_parts = line.split("-")
            time_str = line_parts[-1].strip()
            # Check if there are enough parts to unpack
            if len(time_str) >= 2:
                hours, minutes = map(int, time_str.split(':'))
                # Process the date and hour as needed
                # For example, display them on labels
                #self.content_hours.config(text=f"Last entry hour: {hours}")
                #print(f"Loop hours : {hours}")
                #self.add_hours(hours)
                self.add_minutes(hours*60)
                self.add_minutes(minutes)
                #print(f"Loop minutes : {minutes}")
                
            else:
                # Handle lines without the expected format
                print(f"Skipped line: {line}")
                self.content_hours.config(text=f"Last entry hour: {hours}")
        self.min_to_hour(self.total_minutes)
        self.content_hours.config(text=f"Total logged in time: {self.total_hours}:{self.remaining_minutes}")
        self.required_min_minutes= self.total_lines*2550
        print(f"required min minutes : {self.required_min_minutes}")
        self.acquired_total_minutes = (self.total_hours*60)+self.remaining_minutes
        print(f"acquired total minutes : {self.acquired_total_minutes}")
        self.rest_total_minutes = self.acquired_total_minutes - self.required_min_minutes
        if (self.rest_total_minutes > 0):
            self.status_text = "Acquired is more"
        else:
            self.status_text = "Required is more"
        print(f"rest total minutes : {self.rest_total_minutes}")
        self.rest_hours = self.rest_total_minutes // 60
        print(f"rest_hours = {self.rest_hours}")
        self.rest_minutes = self.rest_total_minutes % 60
        print(f"rest_minutes = {self.rest_minutes}")
        self.rest_time.config(text=f"Total Difference: {self.rest_hours:02d}:{self.rest_minutes:02d} --> {self.status_text}")
        
    def add_hours(self,hours):
        self.total_hours+=hours
        print(f"add_hours : {self.total_hours}")
    def add_minutes(self,minutes):
        self.total_minutes+=minutes
        print(f"add_minutes : {self.total_minutes}")
    def min_to_hour(self,total_minute):
        print(f"TOTAL RECEIVED MIN = {total_minute}")
        self.remaining_minutes = total_minute % 60
        print(f"mod 60 : {self.remaining_minutes}")
        hours_to_add = total_minute // 60
        self.add_hours(hours_to_add)
        print(f"divide by 60 : {hours_to_add}")
        
