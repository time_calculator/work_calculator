#def convert_minute_to_string(self,total_extra):
def convert_minute_to_string(total_extra):
    hours, minutes = divmod(total_extra, 60)
    time_str = "{:02d}:{:02d}".format(int(hours), int(minutes))
    return time_str