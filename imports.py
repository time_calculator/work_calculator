import sys,os
from PyQt5.QtWidgets import QApplication,QCheckBox, QWidget, QLabel, QLineEdit, QPushButton, QVBoxLayout, QMessageBox
from PyQt5.QtGui import QIcon
import re
from datetime import timedelta
from convert_minute_to_string import convert_minute_to_string
from calculate_difference import calculate_difference